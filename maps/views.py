import json, requests

from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.

def calculate(request):

    API_KEY = 'AIzaSyCkkNHVobtXHthQ_T9kSsY8aLZkWk0dCIc'

    # Get/Clean URL query data
    station = ''
    o_address_number = ''
    o_street = ''
    o_zipcode = ''
    o_city = ''
    d_address_number = ''
    d_street = ''

    if 'station' not in request.GET:
        return HttpResponse('Error: no station specified')
    station = request.GET['station']

    if 'o_address_number' not in request.GET:
        return HttpResponse('Error: no origin address number specified')
    o_address_number = request.GET['o_address_number']

    if 'o_street' not in request.GET:
        return HttpResponse('Error: no origin street specified')
    o_street = request.GET['o_street']

    if 'o_zipcode' in request.GET:
        o_zipcode = request.GET['o_zipcode']
    if 'o_city' in request.GET:
        o_city = request.GET['o_city']
    if (not o_zipcode) and (not o_city):
        return HttpResponse('Error: no origin  zipcode or city specified')

    if 'd_address_number' not in request.GET:
        return HttpResponse('Error: no destination address number specified')
    d_address_number = request.GET['d_address_number']

    if 'd_street' not in request.GET:
        return HttpResponse('Error: no destination street specified')
    d_street = request.GET['d_street']

    # Build origin query
    origin_li = [o_address_number, o_street, o_zipcode, o_city]
    origin_li = [x for x in origin_li if x!= '']
    origin_li = [x.replace(' ', '+') for x in origin_li]

    origin_query = '+'.join(origin_li)

    # Build final destination query
    dest_li = [d_address_number, d_street]
    dest_li = [x for x in dest_li if x!= '']
    dest_li = [x.replace(' ', '+') for x in dest_li]
    dest_li.append('Philadelphia+PA')

    dest_query = '+'.join(dest_li)

    # Build station query
    station_query = ''
    if station == 'Lindenwold':
        station_query = '901+N.+Berlin+Road+Lindenwold+NJ+08201'
    elif station == 'Ashland':
        station_query = '2+Burnt+Mill+Road+Voorhees+NJ'
    elif station == 'Woodcrest':
        station_query = '200+Tindale+Drive+Cherry+Hill+NJ'
    elif station == 'Haddonfield':
        station_query = '39.8976532,-75.0393321'
    elif station == 'Westmont':
        station_query = '100+Stoy+Avenue+Haddon+Township+NJ'
    elif station == 'Collingswood':
        station_query = '100+Lees+Avenue+Collingswood+NJ'
    elif station_query == 'Ferry+Avenue':
        station_query = '160+Ferry+Avenue+Camden+NJ'
    elif station_query == 'Broadway':
        station_query = '39.9430562,-75.1192689'
    elif station_query == 'City+Hall':
        station_query = '39.9458129,-75.1210817'
    if not station_query:
        HttpResponse('Error: station could not be found')

    # Build API parameters
    base_url = 'https://maps.googleapis.com/maps/api/directions/json'

    origin_to_dest_url = (base_url
                          + '?origin=' + origin_query
                          + '&destination=' + dest_query
                          + '&key=%s') %(API_KEY)

    origin_to_station_url = (base_url
                             + '?origin=' + origin_query
                             + '&destination=' + station_query
                             + '&key=%s') %(API_KEY)

    station_to_dest_url = (base_url
                           + '?origin=' + station_query
                           + '&destination=' + dest_query
                           + '&mode=transit'
                           + '&key=%s') %(API_KEY)
    
    # Get Google API data
    origin_to_dest = requests.get(origin_to_dest_url)
    origin_to_station = requests.get(origin_to_station_url) 
    station_to_dest = requests.get(station_to_dest_url)

    o2d_data = json.loads(origin_to_dest.text)
    o2s_data = json.loads(origin_to_station.text)
    s2d_data = json.loads(station_to_dest.text)

    # Calculate time from origin to final destination
    o2d_time = o2d_data['routes'][0]['legs'][0]['duration']['value']

    # Calculate time from origin to station
    o2s_time = o2s_data['routes'][0]['legs'][0]['duration']['value']    

    # Calculate time from station to final destinatation
    s2d_steps = s2d_data['routes'][0]['legs'][0]['steps']
    s2d_time = s2d_data['routes'][0]['legs'][0]['duration']['value']
    if s2d_steps[0]['travel_mode'] == 'WALKING':
        # (Eliminate any duration time for walking to train station)
        s2d_time -=  s2d_steps[0]['duration']['value']
    
    # Prepare and return client data
    car_time = o2d_time
    patco_time = o2s_time + s2d_time

    time_data = {'carTime': car_time/60,
                 'patcoTime': patco_time/60}

    return HttpResponse(json.dumps(time_data), content_type='application/json')